class Company < ActiveRecord::Base
 	belongs_to :user, class_name:'User'
  	attr_accessible :date, :members, :pitch_video, :stage, :vision
end
