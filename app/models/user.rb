class User < ActiveRecord::Base
  has_many :investors
  attr_accessible :name, :e_mail, :user_type, :password, :skype_username, :username

 validates_format_of :e_mail,:with=> /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/
  validates :e_mail, :presence => true, 
   :uniqueness => true

 validates :password, :presence => true,
  :confirmation => true,
  :length => {:within => 6..40}

   validates :username, :presence => true,
  :length => {:within => 6..40},
  :uniqueness => true


  def authenticate(password)
  	self.password == password
  end  
end

