class Investor < ActiveRecord::Base
  belongs_to :user, class_name: 'User'
  attr_accessible :businesses_investedin, :income, :name, :past_experience, :type_of_investor
end
