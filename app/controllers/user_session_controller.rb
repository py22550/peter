class UserSessionController < ApplicationController
  def index
  end

  def create
    # find the user with the email
  	user = User.where(e_mail: params[:e_mail].downcase).first

    if user && user.authenticate(params[:password])
      log_in user
  		redirect_to profile_user_path(user)
  	else
  		flash.now[:danger] = "Invaild email/password combination"
      render :index
  	end
  end

  def log_in(user)
    session[:user] = user
  end
end
