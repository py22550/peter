class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :username
      t.string :password
      t.string :e_mail
      t.string :skype_username
      t.string :investor_entrepreneur

      t.timestamps
    end
  end
end
