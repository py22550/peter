class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.text :vision
      t.string :pitch_video
      t.string :stage
      t.string :members
      t.datetime :date

      t.timestamps
    end
  end
end
