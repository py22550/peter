class CreateInvestors < ActiveRecord::Migration
  def change
    create_table :investors do |t|
      t.string :name
      t.string :past_experience
      t.string :businesses_investedin
      t.string :type_of_investor
      t.integer :income

      t.timestamps
    end
  end
end
